// Copyright 2019 ADLINK Technology, Inc.
// Developer: Alan Chen (alan.chen@adlinktech.com)
// Developer: YuSheng Tseng (chester.tseng@adlinktech.com)
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/string.hpp"
#include <chrono>
#include <memory>

class TransportPriorityNode : public rclcpp::Node {
  public:
  TransportPriorityNode()
      : Node("transport_priority")
  {
    rclcpp::SystemDefaultsQoS qos;
    qos.transport_priority(100);
    pub_ = this->create_publisher<std_msgs::msg::String>
      ("topic_name", qos);
  }

  private:
  rclcpp::Publisher<std_msgs::msg::String>::SharedPtr pub_;
};

int main(int argc, char* argv[])
{
  rclcpp::init(argc, argv);
  auto node = std::make_shared<TransportPriorityNode>();
  rclcpp::spin(node);
  rclcpp::shutdown();
  return 0;
}
